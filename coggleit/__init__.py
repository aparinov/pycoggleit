import json
import pprint
import requests
from requests.auth import HTTPBasicAuth
import webbrowser
import re
import csv
import time
from datetime import datetime as dt



# https://openpyxl.readthedocs.io/en/default/api/openpyxl.utils.cell.html
# https://openpyxl.readthedocs.io/en/default/api/openpyxl.cell.cell.html
from openpyxl import *
from openpyxl.utils import * #column_index_from_string,get_column_letter

import pprint
pp = pprint.PrettyPrinter(indent=4)

base_dir = "C:\\Users\\aparinov\\OneDrive\\InCards\\_files\\"
target_dir = "\\20180314-1\\"

class PyCoggleit():

    def __init__(self,app_name,client_id,client_secret):
        self.app_name = app_name
        self.client_id = client_id
        self.client_secret = client_secret
        self.url_base = "https://coggle.it/"
        self.access_token=""
        self.texts=[]
        self.diagram_id =""

    def authenticate(self):
        # Get code
        prms = {'response_type': "code", 'scope': "read", 'client_id': self.client_id, 'redirect_uri': 'http://localhost'}
        path = self.url_base + 'dialog/authorize/'
        resp = requests.get(path, params=prms)
        webbrowser.open_new_tab(resp.url)
        print("Please copy code")
        code = input()

        # Get access token
        prms = {"code": code, "grant_type": "authorization_code", "redirect_uri": "http://localhost"}
        resp1 = requests.post(self.url_base + "token", auth=HTTPBasicAuth(self.client_id, self.client_secret), json=prms)
        d1 = json.loads(resp1.text)
        self.access_token = d1["access_token"]
        if self.access_token!="":
            return True
        return False

    def diagram_metrics(self,diagram_id):
        try:
            url_nodes = "api/1/diagrams/" + diagram_id + "/nodes/"
            resp = requests.get(self.url_base + url_nodes, params={"access_token": self.access_token})
            if resp.status_code == 200:

                node = json.loads(resp.text)[0]
                # pp.pprint(node)
                self.texts=[]
                self.words = []
                m1 = {"max_height": 0, "count_first_layer_branches": 0}
                m1["max_height"] = self.max_height(node)
                m1["count_first_layer_branches"] = self.count_first_layer_branches(node)
                m2 = self.count_assoc()
                metrics = {**m1, **m2}
                metrics["count_nodes"] = len(self.texts)
                self.get_words(node)
                metrics["words"] = "\n".join(self.words)
            else:
                metrics = {"error":"Error 403: Permission denied"}
        except Exception as e:
            metrics = {"error":str(e)}
        return metrics

    def max_height(self,node):
        self.texts.append(node["text"])
        chls = node.get("children")
        if (chls == None) | (len(chls) == 0):
            return 0
        heights = []
        for i in node["children"]:
            heights.append(self.max_height(i))
        return 1 + max(heights)

    def get_words(self, node):
        self.words.append(node["text"])
        for i in node["children"]:
            self.get_words(i)




    def count_first_layer_branches(self,node):
        chls = node.get("children")
        if (chls == None) | (len(chls) == 0):
            return 0
        return len(chls)


    def count_assoc(self):
        ret_dict = {"associations": 0, "images": 0}
        if len(self.texts) == 0:
            return ret_dict
        patterns = {"associations": r"\[#\]\(#([a-zA-Z0-9]+)\)",
                    "images": r"!\[([a-zA-Z0-9 ]*)\]\(https?:"}
        for text in self.texts:
            for key in patterns.keys():
                objs = re.findall(patterns[key], text)
                if objs:
                    ret_dict[key] = ret_dict[key] + len(objs)
        return ret_dict


    def metrics_xlsx(self,sourcefile, source_list, source_column_letter, target_column_letter=None):
        if sourcefile == None:
            return "No file"
        wb = load_workbook(sourcefile)
        ws_source = wb.get_sheet_by_name(source_list)
        if target_column_letter==None:
            timestamp = dt.now().strftime("%H%M%S")
            ws_target = wb.create_sheet(source_list+"-"+timestamp,0) #insert at first position
            ws_target["A1"].value = "name"
            ws_target["B1"].value = "diagram"
            ws_target["C1"].value = "associations"
            ws_target["D1"].value = "max_height"
            ws_target["E1"].value = "count_nodes"
            ws_target["F1"].value = "count_first_layer_branches"
            ws_target["G1"].value = "images"
        else:
            ws_target=ws_source

        source_column_index = column_index_from_string(source_column_letter)
        #c = ws_source.columns[source_column_index]
        cell_generator = [ cll for cll in ws_source.iter_cols(min_col=source_column_index-1, max_col=source_column_index)]

        #target_column = ws.columns[target_column_index]

        for i in cell_generator[1]:

            if i.value is not None:
                obj = re.search(r"https?:\/\/coggle.it\/diagram\/([_a-zA-Z0-9 -]+)", i.value)
                if obj:
                    self.diagram_id = obj.group(1)
                    dr = self.diagram_metrics(self.diagram_id)
                    if (target_column_letter == None) & ("error" not in dr.keys()):
                        name = cell_generator[0][i.row-1].value
                        ws_target["A" + str(i.row)].value = name
                        ws_target["B"+ str(i.row)].value = i.value
                        ws_target["C" + str(i.row)].value = dr["associations"]
                        ws_target["D" + str(i.row)].value = dr["max_height"]
                        ws_target["E" + str(i.row)].value = dr["count_nodes"]
                        ws_target["F" + str(i.row)].value = dr["count_first_layer_branches"]
                        ws_target["G" + str(i.row)].value = dr["images"]
                        with open(base_dir+target_dir+ name+".txt","w",encoding="utf-8") as f:
                            f.write(dr["words"])
                    else:
                        ws_target["A" + str(i.row)].value = cell_generator[0][i.row-1].value
                        ws_target["B" + str(i.row)].value = i.value
                        txt = json.dumps(dr)
                        ws_target["C" + str(i.row)].value = txt
        wb.save(sourcefile)

    # выводит результат вычислений в виде текста в одной строке
    def metrics_xlsx2(self,sourcefile, source_list, source_column_letter, target_column_letter=None):
        if sourcefile == None:
            return "No file"
        wb = load_workbook(sourcefile)
        ws_source = wb.get_sheet_by_name(source_list)
        if target_column_letter==None:
            timestamp = dt.now().strftime("%I%M")
            ws_target = wb.create_sheet(source_list+"-"+timestamp,0) #insert at first position
        else:
            ws_target=ws_source

        source_column_index = column_index_from_string(source_column_letter) - 1
        c = ws_source.columns[source_column_index]

        #target_column = ws.columns[target_column_index]

        for i in c:
            if i.value is not None:
                obj = re.search(r"https?:\/\/coggle.it\/diagram\/([_a-zA-Z0-9 -]+)", i.value)
                if obj:
                    self.diagram_id = obj.group(1)
                    txt = json.dumps(self.diagram_metrics(self.diagram_id))
                    print(txt)
                    if target_column_letter == None:
                        ws_target["A"+ str(i.row)].value = i.value
                        ws_target["B" + str(i.row)].value = txt
                    else:
                        ws_target[target_column_letter + str(i.row)].value = txt
        #wb.save(sourcefile)



app_name = "testapp1"
client_id = "58a09e26531f8d000198183e"
client_secret = "81f471decd8d83e2d333abdcd77af9534618c353cd6211355be00008cc140f29"

client = PyCoggleit(app_name,client_id,client_secret)
if client.authenticate() == True:
        #client.metrics_xlsx("C:\\Dropbox\\proj\\pycoggleit\\оценки2017_2.xlsx","Sheet1","C")
        #client.metrics_xlsx(base_dir+"оценки_10_11.xlsx", "Лист2", "B")
        client.metrics_xlsx(base_dir + "20180323.xlsx", "Лист1", "B")